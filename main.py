from typing import Union

from fastapi import FastAPI
from pydantic import BaseModel
import requests

from dao.req import Item, post_Item, Touch_Para
from data_operation import sql

from utils.utils import print_test
from workorder.opsflow import create_workorder

app = FastAPI()


# 测试接口1
#
#
#
@app.get("/")
def read_root():
    result: tuple = sql.mysql_select()
    # print(result)
    r = requests.get("https://www.baidu.com")
    # print(r)
    return {"Hello": "World\n", 'result': result}
    # return {"Hello": "World\n"}


# 测试接口2
#
#
#
@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}


# 测试接口3
#
#
#
@app.put("/items/{item_id}")
def update_item(item_id: int, item: Item):
    return {"item_id": item_id, "item_name": item.name, }


# 测试接口4
#
#
#
@app.post("/post_items/{item_id}")
def insert_data(item_id: int, item: post_Item):
    print_test(item)
    return {"item_id": item_id,
            "post_item_id": item.id,
            "post_item_name": item.name,
            "post_item_price": item.price,
            "post_item_score": item.score,
            "post_item_is_offer": item.is_offer
            }


# 触发接口
@app.post("/touchoff")
def insert_data(touchpara: Touch_Para):
    # 创建工单
    status = create_workorder(touchpara)

    # 去回写需求系统的库，把status字段更改
    # print(type(status))
    # for i in status:
    #     print(i)
    result = sql.mysql_update_status(status[1].status)

    return {"status": status, "result": result}

    # return {"project": touchpara.project,
    #         "service": touchpara.service,
    #         "status_code": touchpara.status_code,
    #         "status": touchpara.status,
    #         "is_offer": touchpara.is_offer
    #         }
