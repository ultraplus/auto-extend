from typing import Union

from pydantic import BaseModel


class Item(BaseModel):
    name: str
    price: float
    is_offer: Union[bool, None] = None


class post_Item(BaseModel):
    id: int
    name: str
    price: float
    score: float
    is_offer: Union[bool, None] = None


class Touch_Para(BaseModel):
    project: str
    service: str
    status_code: int
    status: str
    is_offer: Union[bool, None] = None
