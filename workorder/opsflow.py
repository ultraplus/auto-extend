from dao.req import Touch_Para


def create_workorder(workorder: Touch_Para):
    """
    Create a workorder
    :param workorder:
    此处为创建工单系统，调取接口主函数
    需要返回自动化扩容状态
    """
    print('Creating workorder...')
    print('Workorder:', workorder)
    return True, workorder
