# 导入pymysql模块
import pymysql

conn = pymysql.connect(
    host='118.31.35.90',  # 主机名（或IP地址）
    port=3306,  # 端口号，默认为3306
    user='root',  # 用户名
    password='123456',  # 密码
    charset='utf8mb4'  # 设置字符编码
)

# 获取mysql服务信息（测试连接，会输出MySQL版本号）
print(conn.get_server_info())

# 创建游标对象
cursor = conn.cursor()

# 选择数据库
conn.select_db("pytest")


def mysql_select() -> tuple:
    # 建立数据库连接

    # 执行查询操作
    cursor.execute('SELECT * FROM test_table')

    # 获取查询结果，返回元组
    result: tuple = cursor.fetchall()
    # print(result)

    return result


def mysql_update_status(stauts: str) -> tuple:
    # 建立数据库连接

    # 执行查询操作
    cursor.execute(f"update test_table set project='哈哈哈lizi哈' where id = 1;")

    # 获取查询结果，返回元组
    result: tuple = cursor.fetchall()
    # print(result)

    return result


def mysql_disconnect() -> str:
    # 关闭游标和连接
    cursor.close()
    conn.close()
    return "the database disconnected"
